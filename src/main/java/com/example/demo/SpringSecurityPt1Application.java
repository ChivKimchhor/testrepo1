package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSecurityPt1Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringSecurityPt1Application.class, args);
    }

}
