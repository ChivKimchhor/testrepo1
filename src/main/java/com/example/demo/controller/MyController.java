package com.example.demo.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyController {
    @GetMapping("/")
    public String home(){
        return "hello world";
    }
    @GetMapping("/user")
    public String user(){
        return "this is user";
    }
    @GetMapping("/admin")
    public String admin(){
        return "this is admin";
    }
}
